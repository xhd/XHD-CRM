﻿/*
* MSG_Task_follow.cs
*
* 功 能： N/A
* 类 名： MSG_Task_follow
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/7/28 16:09:44    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
namespace XHD.Model
{
    /// <summary>
    /// MSG_Task_follow:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class MSG_Task_follow
    {
        public MSG_Task_follow()
        { }
        #region Model
        private int _id;
        private int? _task_id;
        private int? _follow_id;
        private DateTime? _follow_time;
        private string _follow_content;
        private int? _follow_status;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? task_id
        {
            set { _task_id = value; }
            get { return _task_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? follow_id
        {
            set { _follow_id = value; }
            get { return _follow_id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime? follow_time
        {
            set { _follow_time = value; }
            get { return _follow_time; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string follow_content
        {
            set { _follow_content = value; }
            get { return _follow_content; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? follow_status
        {
            set { _follow_status = value; }
            get { return _follow_status; }
        }
        #endregion Model

    }
}

