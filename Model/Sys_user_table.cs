﻿/*
* Sys_user_table.cs
*
* 功 能： N/A
* 类 名： Sys_user_table
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/8/1 10:38:12    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_user_table:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_user_table
    {
        #region Model

        private int? _column_id;
        private int? _column_width;
        private int? _emp_id;
        private int? _ishide;
        private int? _menu_id;

        /// <summary>
        /// </summary>
        public int? emp_id
        {
            set { _emp_id = value; }
            get { return _emp_id; }
        }

        /// <summary>
        /// </summary>
        public int? menu_id
        {
            set { _menu_id = value; }
            get { return _menu_id; }
        }

        /// <summary>
        /// </summary>
        public int? column_id
        {
            set { _column_id = value; }
            get { return _column_id; }
        }

        /// <summary>
        /// </summary>
        public int? isHide
        {
            set { _ishide = value; }
            get { return _ishide; }
        }

        /// <summary>
        /// </summary>
        public int? column_width
        {
            set { _column_width = value; }
            get { return _column_width; }
        }

        #endregion Model
    }
}