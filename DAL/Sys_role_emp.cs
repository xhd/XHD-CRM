/*
* Sys_role_emp.cs
*
* 功 能： N/A
* 类 名： Sys_role_emp
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:Sys_role_emp
    /// </summary>
    public class Sys_role_emp
    {
        #region  Method

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public void Add(Model.Sys_role_emp model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into Sys_role_emp(");
            strSql.Append("RoleID,empID)");
            strSql.Append(" values (");
            strSql.Append("@RoleID,@empID)");
            SqlParameter[] parameters =
            {
                new SqlParameter("@RoleID", SqlDbType.Int, 4),
                new SqlParameter("@empID", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.RoleID;
            parameters[1].Value = model.empID;

            DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.Sys_role_emp model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update Sys_role_emp set ");
            strSql.Append("RoleID=@RoleID,");
            strSql.Append("empID=@empID");
            strSql.Append(" where ");
            SqlParameter[] parameters =
            {
                new SqlParameter("@RoleID", SqlDbType.Int, 4),
                new SqlParameter("@empID", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.RoleID;
            parameters[1].Value = model.empID;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(string strWhere)
        {
            //该表无主键信息，请自定义主键/条件字段
            var strSql = new StringBuilder();
            strSql.Append("delete from Sys_role_emp ");
            strSql.Append(" where " + strWhere);
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }


        /// <summary>
        ///     得到一个对象实体
        /// </summary>
        public Model.Sys_role_emp GetModel()
        {
            //该表无主键信息，请自定义主键/条件字段
            var strSql = new StringBuilder();
            strSql.Append("select  top 1 RoleID,empID from Sys_role_emp ");
            strSql.Append(" where ");
            SqlParameter[] parameters =
            {
            };

            var model = new Model.Sys_role_emp();
            DataSet ds = DbHelperSQL.Query(strSql.ToString(), parameters);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows[0]["RoleID"] != null && ds.Tables[0].Rows[0]["RoleID"].ToString() != "")
                {
                    model.RoleID = int.Parse(ds.Tables[0].Rows[0]["RoleID"].ToString());
                }
                if (ds.Tables[0].Rows[0]["empID"] != null && ds.Tables[0].Rows[0]["empID"].ToString() != "")
                {
                    model.empID = int.Parse(ds.Tables[0].Rows[0]["empID"].ToString());
                }
                return model;
            }
            return null;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("select RoleID,empID ");
            strSql.Append(" FROM Sys_role_emp ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(" RoleID,empID ");
            strSql.Append(" FROM Sys_role_emp ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql = new StringBuilder();
            var strSql1 = new StringBuilder();
            strSql.Append("select ");
            strSql.Append(" top " + PageSize + " * FROM Sys_role_emp ");
            strSql.Append(" WHERE id not in ( SELECT top " + (PageIndex - 1)*PageSize + " id FROM Sys_role_emp ");
            strSql.Append(" where " + strWhere + " order by " + filedOrder + " ) ");
            strSql1.Append(" select count(id) FROM Sys_role_emp ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" and " + strWhere);
                strSql1.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            Total = DbHelperSQL.Query(strSql1.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql.ToString());
        }

        #endregion  Method
    }
}