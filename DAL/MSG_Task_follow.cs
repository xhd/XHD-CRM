﻿/*
* MSG_Task_follow.cs
*
* 功 能： N/A
* 类 名： MSG_Task_follow
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015/7/28 16:09:44    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:MSG_Task_follow
    /// </summary>
    public class MSG_Task_follow
    {
        #region  BasicMethod

        /// <summary>
		/// 增加一条数据
		/// </summary>
		public int Add(XHD.Model.MSG_Task_follow model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("insert into MSG_Task_follow(");
            strSql.Append("task_id,follow_id,follow_time,follow_content,follow_status)");
            strSql.Append(" values (");
            strSql.Append("@task_id,@follow_id,@follow_time,@follow_content,@follow_status)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters = {
                    new SqlParameter("@task_id", SqlDbType.Int,4),
                    new SqlParameter("@follow_id", SqlDbType.Int,4),
                    new SqlParameter("@follow_time", SqlDbType.DateTime),
                    new SqlParameter("@follow_content", SqlDbType.NVarChar,-1),
                    new SqlParameter("@follow_status", SqlDbType.Int,4)};
            parameters[0].Value = model.task_id;
            parameters[1].Value = model.follow_id;
            parameters[2].Value = model.follow_time;
            parameters[3].Value = model.follow_content;
            parameters[4].Value = model.follow_status;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            else
            {
                return Convert.ToInt32(obj);
            }
        }

        /// <summary>
        /// 更新一条数据
        /// </summary>
        public bool Update(XHD.Model.MSG_Task_follow model)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("update MSG_Task_follow set ");
            strSql.Append("follow_content=@follow_content");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters = {
                    new SqlParameter("@follow_content", SqlDbType.NVarChar,-1),
                    new SqlParameter("@id", SqlDbType.Int,4)};
            parameters[0].Value = model.follow_content;
            parameters[1].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from MSG_Task_follow ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from MSG_Task_follow ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteWhere(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from MSG_Task_follow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }
        /// <summary>
		/// 获得数据列表
		/// </summary>
		public DataSet GetList(string strWhere)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select id,task_id,follow_id,follow_time,follow_content,follow_status ");
            strSql.Append(" FROM MSG_Task_follow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top.ToString());
            }
            strSql.Append(" id,task_id,follow_id,follow_time,follow_content,follow_status ");
            strSql.Append(" FROM MSG_Task_follow ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        /// 分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            StringBuilder strSql_grid = new StringBuilder();
            StringBuilder strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM MSG_Task_follow ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append("      id,task_id,follow_id,follow_time,follow_content,follow_status ");
            strSql_grid.Append(" FROM ( SELECT id,task_id,follow_id,follow_time,follow_content,follow_status, ROW_NUMBER() OVER( Order by " + filedOrder + " ) AS n from MSG_Task_follow");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize * (PageIndex - 1) + " AND " + PageSize * PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        #endregion  ExtensionMethod
    }
}