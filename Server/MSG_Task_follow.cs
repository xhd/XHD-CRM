﻿/*
* MSG_Task_follow.cs
*
* 功 能： N/A
* 类 名： MSG_Task_follow
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Text;
using System.Web;
using XHD.BLL;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class MSG_Task_follow
    {
        public static BLL.MSG_Task_follow TaskFollow = new BLL.MSG_Task_follow();
        public static Model.MSG_Task_follow model = new Model.MSG_Task_follow();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;

        public MSG_Task_follow()
        {
        }

        public MSG_Task_follow(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        //save
        public void save()
        {
            model.follow_content = PageValidate.InputText(request["T_follow"], int.MaxValue);

            string id = request["followid"];

            if (PageValidate.IsNumber(id))
            {
                model.id = int.Parse(id);
                TaskFollow.Update(model);
            }
            else
            {
                string task_id = request["taskid"];
                string follow_status = request["statuid"];

                if ( PageValidate.IsNumber(task_id))
                {
                    if (follow_status != "0")
                        follow_status = "1";
                    model.follow_id = emp_id;
                    model.task_id = int.Parse(task_id);
                    model.follow_status = int.Parse(follow_status);
                    model.follow_time = DateTime.Now;

                    TaskFollow.Add(model);

                    if (follow_status == "1")
                    {
                        MSG_Task task = new MSG_Task();
                        task.UpdateStatu(int.Parse(task_id), int.Parse(follow_status));
                    }
                }
            }
        }
        //serch
        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " id ";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = " desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total;

            string serchtxt = "1=1";

            if (PageValidate.IsNumber(request["taskid"]))
                serchtxt += " and task_id=" + int.Parse(request["taskid"]);

            DataSet ds = TaskFollow.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            string dt = GetGridJSON.DataTableToJSON1(ds.Tables[0], Total);
            return (dt);
        }
        public string form(string id)
        {
            string dt;
            if (PageValidate.IsNumber(id))
            {
                DataSet ds = TaskFollow.GetList(string.Format("id = {0}", int.Parse(id)));
                dt = DataToJson.DataToJSON(ds);
            }
            else
            {
                dt = "{}";
            }
            return dt;
        }
        //del
        public string del(int id)
        {
            bool canDel = false;

            if (uid == "admin")
            {
                canDel = true;
            }
            else
            {
                var getauth = new GetAuthorityByUid();
                canDel = getauth.GetBtnAuthority(emp_id.ToString(), "112");
                if (!canDel)
                    return ("auth");
            }

            if (canDel)
            {
                DataSet ds = TaskFollow.GetList("id=" + id);

                string EventType = "删除任务跟进";

                bool isdel = TaskFollow.Delete(id);
                if (isdel)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int UserID = emp_id;
                        string UserName = emp_name;
                        string IPStreet = request.UserHostAddress;
                        int EventID = id;
                        string EventTitle = ds.Tables[0].Rows[i]["task_title"].ToString();

                        var log = new sys_log();
                        log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);
                    }
                    return ("true");                   
                }
                return ("false");
            }
            return ("auth");
        }
    }
}
